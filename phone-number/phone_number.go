package phonenumber

import (
	"errors"
	"fmt"
	"regexp"
)

func Number(phoneNumber string) (string, error) {
	// Remove all non-numeric characters
	phoneNumber = regexp.MustCompile(`[^0-9]*`).ReplaceAllString(phoneNumber, "")

	// Remove 1 prefix if present
	if len(phoneNumber) == 11 && phoneNumber[0] == '1' {
		phoneNumber = phoneNumber[1:11]
	}

	// Validate the parsed number is in format NXXNXXXXXX where N is 2-9 and X is 0-9
	if !regexp.MustCompile(`^([2-9][0-9]{2}){2}([0-9]{4})$`).MatchString(phoneNumber) {
		return "", errors.New("malformed phone number, must be in format NXXNXXXXXX")
	}

	return phoneNumber, nil
}

func AreaCode(phoneNumber string) (string, error) {
	// Parse the phone number
	phoneNumber, err := Number(phoneNumber)

	// If the provided number can't be validated, return an error
	if err != nil {
		return "", err
	}

	// Return first three characters of number which is the area code
	return phoneNumber[:3], nil
}

func Format(phoneNumber string) (string, error) {
	// Parse the phone number
	phoneNumber, err := Number(phoneNumber)

	// If the provided number can't be validated, return an error
	if err != nil {
		return "", err
	}

	// Return provided number formatted as '(NXX) NXX-XXXX'
	return fmt.Sprintf("(%s) %s-%s", phoneNumber[:3], phoneNumber[3:6], phoneNumber[6:]), nil
}
