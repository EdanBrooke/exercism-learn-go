package parsinglogfiles

import (
	"fmt"
	"regexp"
)

func IsValidLine(text string) bool {
	re := regexp.MustCompile(`^\[(TRC|DBG|INF|WRN|ERR|FTL)\]`)
	return re.MatchString(text)
}

func SplitLogLine(text string) []string {
	re := regexp.MustCompile(`<(\W*?)>`)
	return re.Split(text, -1)
}

func CountQuotedPasswords(lines []string) (count int) {
	re := regexp.MustCompile(`(?i)".*password.*"`)
	for _, line := range lines {
		count += len(re.FindAllStringSubmatch(line, -1))
	}
	return
}

func RemoveEndOfLineText(text string) string {
	re := regexp.MustCompile(`end-of-line\d+`)
	return re.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) (output []string) {
	re := regexp.MustCompile(`User\s+([a-zA-Z0-9]+)\s`)
	for _, line := range lines {
		match := re.FindStringSubmatch(line)
		if len(match) >= 1 {
			username := match[1]
			line = fmt.Sprintf("[USR] %s %s", username, line)
		}
		output = append(output, line)
	}
	return
}
