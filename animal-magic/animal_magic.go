package chance

import "math/rand"

// RollADie returns a random int d with 1 <= d <= 20.
func RollADie() int {
	// Get a number between 1 and 20.
	// rand.Intn returns starting at 0, so increment the result by 1.
	return rand.Intn(19) + 1
}

// GenerateWandEnergy returns a random float64 f with 0.0 <= f < 12.0.
func GenerateWandEnergy() float64 {
	// Get a floating point number between 0.0 and 12.0.
	// rand.Float64 returns between 0.0 and 1.0, so multiply result by 12.
	return rand.Float64() * 12
}

// ShuffleAnimals returns a slice with all eight animal strings in random order.
func ShuffleAnimals() []string {
	animals := []string{"ant", "beaver", "cat", "dog", "elephant", "fox", "giraffe", "hedgehog"}
	rand.Shuffle(len(animals), func(x, y int) {
		animals[x], animals[y] = animals[y], animals[x]
	})
	return animals
}
