package space

import (
	"errors"
	"strings"
)

type Planet string

func GetOrbitalPeriodFactor(planet Planet) (float64, error) {
	switch strings.ToLower(string(planet)) {
	case "mercury":
		return 0.2408467, nil
	case "venus":
		return 0.61519726, nil
	case "mars":
		return 1.8808158, nil
	case "jupiter":
		return 11.862615, nil
	case "saturn":
		return 29.447498, nil
	case "uranus":
		return 84.016846, nil
	case "neptune":
		return 164.79132, nil
	case "earth":
		return 1, nil
	default:
		return 0, errors.New("invalid planet: " + string(planet))
	}
}

func Age(seconds float64, planet Planet) float64 {
	orbitalPeriodFactor, err := GetOrbitalPeriodFactor(planet)
	if err == nil {
		return (seconds / 31557600) / orbitalPeriodFactor
	} else {
		return -1
	}
}
