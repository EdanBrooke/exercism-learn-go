package hamming

import "errors"

func Distance(a, b string) (distance int, err error) {
	if len(a) != len(b) {
		err = errors.New("strings must be of equal length")
		return
	}
	for index, char := range a {
		if char != rune(b[index]) {
			distance++
		}
	}
	return
}
