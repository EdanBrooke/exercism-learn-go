package lasagna

// PreparationTime calculates the time taken to prepare a lasagna based on the number of layers
// and the preparation time per layer.
func PreparationTime(layers []string, layerPrepTime int) int {
	if layerPrepTime <= 0 {
		layerPrepTime = 2
	}

	return len(layers) * layerPrepTime
}

// Quantities determines the quantities of noodles and sauce required to make a lasagna
// based on the given layers.
func Quantities(layers []string) (noodles int, sauce float64) {
	const noodlesPerLayer int = 50
	const saucePerLayer float64 = 0.2

	noodles = 0
	sauce = 0.0

	for i := 0; i < len(layers); i++ {
		if layers[i] == "sauce" {
			sauce += saucePerLayer
		} else if layers[i] == "noodles" {
			noodles += noodlesPerLayer
		}
	}

	return
}

// AddSecretIngredient appends the final item of friendsIngredients to ingredients.
func AddSecretIngredient(friendsIngredients []string, ingredients []string) {
	secretIngredient := friendsIngredients[len(friendsIngredients)-1:][0]
	ingredients[len(ingredients)-1] = secretIngredient
}

// ScaleRecipe scales the given quantities to the specified number of portions.
func ScaleRecipe(quantitiesForTwo []float64, portions int) (quantities []float64) {
	// Initialise quantities slice as the same length as original quantitiesForTwo slice.
	quantities = make([]float64, len(quantitiesForTwo))
	// Reduce number of portions by half as original quantities are for two portions.
	portionsAsFloat := float64(portions) / 2
	// Iterate original quantities, append new calculated amounts to quantities slice.
	for i := 0; i < len(quantitiesForTwo); i++ {
		quantities[i] = quantitiesForTwo[i] * portionsAsFloat
	}

	return
}
