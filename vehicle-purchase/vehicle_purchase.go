package purchase

import "fmt"

// NeedsLicense determines whether a license is needed to drive a type of vehicle. Only "car" and "truck" require a license.
func NeedsLicense(kind string) bool {
	return kind == "car" || kind == "truck"
}

// ChooseVehicle recommends a vehicle for selection. It always recommends the vehicle that comes first in lexicographical order.
func ChooseVehicle(option1, option2 string) string {
	recommendation := func() string {
		if option1 < option2 {
			return option1
		}
		return option2
	}()
	return fmt.Sprintf("%s is clearly the better choice.", recommendation)
}

// CalculateResellPrice calculates how much a vehicle can resell for at a certain age.
func CalculateResellPrice(originalPrice, age float64) float64 {
	if age < 3 {
		// Vehicle is less than 3 years old, value = 80%.
		return originalPrice * .8
	}

	if age < 10 {
		// Vehicle is between 3 and 10 years old, value = 70%.
		return originalPrice * .7
	}

	// Vehicle is over 10 years old, value = 50%.
	return originalPrice * .5
}
