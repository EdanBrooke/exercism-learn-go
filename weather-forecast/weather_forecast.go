// Package weather provides weather forecast information.
package weather

// CurrentCondition represents the current weather conditions in the given city.
var CurrentCondition string

// CurrentLocation represents the city name for which the forecast is generated.
var CurrentLocation string

// Forecast returns a formatted string representing the current location and weather conditions.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
