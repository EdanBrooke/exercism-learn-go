package isogram

import "unicode"

func IsIsogram(word string) bool {
	dict := make(map[rune]int)
	for _, c := range word {
		c = unicode.ToUpper(c)
		_, exists := dict[c]
		if exists {
			return false
		}
		if unicode.IsLetter(c) {
			dict[c] = 1
		}
	}
	return true
}
