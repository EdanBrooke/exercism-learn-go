package collatzconjecture

import "errors"

func CollatzConjecture(n int) (i int, err error) {
	if n <= 0 {
		err = errors.New("integer must be greater than zero")
		return
	}
	for n != 1 {
		if n%2 == 0 {
			n = n / 2
		} else {
			n = n*3 + 1
		}
		i++
	}
	return
}
