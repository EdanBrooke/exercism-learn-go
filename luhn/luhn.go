package luhn

import (
	"regexp"
	"strconv"
	"strings"
)

func Valid(id string) bool {
	// Strip all white space from provided input
	id = strings.ReplaceAll(id, " ", "")

	// All valid IDs are at least two digits in length
	if len(id) < 2 {
		return false
	}

	// Check for non-numeric characters, input is not valid if any are found
	if regexp.MustCompile(`[^0-9]`).MatchString(id) {
		return false
	}

	// Remaining string is now numeric only, iterate the numbers
	var i, sum int
	for _, r := range id {
		v, _ := strconv.Atoi(string(r))
		if (len(id)-i-2)%2 == 0 {
			v = v * 2
			if v > 9 {
				v = v - 9
			}
		}
		sum = sum + v
		i++
	}

	return sum%10 == 0
}
