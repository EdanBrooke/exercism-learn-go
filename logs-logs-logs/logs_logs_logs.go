package logs

import "unicode/utf8"

// Application identifies the application emitting the given log.
func Application(log string) string {
	for _, r := range log {
		switch r {
		case 10071:
			return "recommendation"
		case 128269:
			return "search"
		case 9728:
			return "weather"
		}
	}
	return "default"
}

// Replace replaces all occurrences of old with new, returning the modified log
// to the caller.
func Replace(log string, oldRune, newRune rune) string {
	// Build a new empty string and iterate the old string.
	// If the rune matches the rune to be replaced, append the replacement rune to the string.
	// Otherwise, append the existing rune to the string.
	newLog := ""
	for _, r := range log {
		if r == oldRune {
			newLog += string(newRune)
		} else {
			newLog += string(r)
		}
	}
	return newLog
}

// WithinLimit determines whether or not the number of characters in log is
// within the limit.
func WithinLimit(log string, limit int) bool {
	// len() will not work here as len() counts bytes, not runes.
	return utf8.RuneCountInString(log) <= limit
}
