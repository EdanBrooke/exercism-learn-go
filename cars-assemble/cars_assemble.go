package cars

// CalculateWorkingCarsPerHour calculates how many working cars are
// produced by the assembly line every hour.
func CalculateWorkingCarsPerHour(productionRate int, successRate float64) float64 {
	return float64(productionRate) * (successRate / 100)
}

// CalculateWorkingCarsPerMinute calculates how many working cars are
// produced by the assembly line every minute.
func CalculateWorkingCarsPerMinute(productionRate int, successRate float64) int {
	return int(float64(productionRate) * (successRate / 100) / 60)
}

// CalculateCost works out the cost of producing the given number of cars.
func CalculateCost(carsCount int) uint {
	// batchProductionCost defines the cost in US dollars of manufacturing
	// the number of cars defined in batchSize.
	const batchProductionCost uint = 95000
	// batchSize defines the number of cars in a single batch.
	const batchSize uint = 10
	// carProductionCost defines the cost of producing a single car without
	// batch production.
	const carProductionCost uint = 10000
	// batches is a calculation of the number of batches produced based on
	// carsCount and batchSize.
	batches := uint(carsCount) / batchSize
	// remainder is the remaining number of cars produced that aren't considered
	// part of a batch.
	remainder := uint(carsCount) % batchSize

	return (batches * batchProductionCost) + (remainder * carProductionCost)
}
