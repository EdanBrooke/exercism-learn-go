package pangram

import "strings"

func IsPangram(input string) bool {
	// Iterate over all characters of the English alphabet, return false if not found in the
	// submitted string. If all characters are found at least once, return true.
	for _, r := range "abcdefghijklmnopqrstuvwxyz" {
		if !strings.ContainsRune(strings.ToLower(input), r) {
			return false
		}
	}

	return true
}
