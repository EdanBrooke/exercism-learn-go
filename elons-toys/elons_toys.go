package elon

import "fmt"

// It may be more efficient using a pointer receiver ONLY in the Drive method.
// However, I have decided to use a pointer in all methods for API consistency.

// Drive moves the car once if there is enough battery to do so.
func (car *Car) Drive() {
	if car.battery >= car.batteryDrain {
		car.distance += car.speed
		car.battery -= car.batteryDrain
	}
}

// DisplayDistance describes how far the car has driven in metres.
func (car *Car) DisplayDistance() string {
	return fmt.Sprintf("Driven %d meters", car.distance)
}

// DisplayBattery describes the car's remaining battery percentage.
func (car *Car) DisplayBattery() string {
	return fmt.Sprintf("Battery at %d%%", car.battery)
}

// CanFinish determines if the car can complete a track based on distance
// and the car's remaining battery & battery drain characteristics.
func (car *Car) CanFinish(trackDistance int) bool {
	return (car.battery/car.batteryDrain)*car.speed >= trackDistance
}
