package reverse

import (
	"slices"
	"strings"
)

func Reverse(input string) string {
	inputElems := strings.Split(input, "")
	slices.Reverse(inputElems)
	return strings.Join(inputElems, "")
}
