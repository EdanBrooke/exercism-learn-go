package gigasecond

import "time"

const GIGASECOND = time.Second * 1e9

func AddGigasecond(t time.Time) time.Time {
	return t.Add(GIGASECOND)
}
