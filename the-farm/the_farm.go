package thefarm

import (
	"errors"
	"fmt"
)

type InvalidCowsError struct {
	numberOfCows int
	message      string
}

func (e *InvalidCowsError) Error() string {
	if e.numberOfCows < 0 {
		e.message = "there are no negative cows"
	}
	if e.numberOfCows == 0 {
		e.message = "no cows don't need food"
	}

	return fmt.Sprintf("%d cows are invalid: %s", e.numberOfCows, e.message)
}

func DivideFood(fc FodderCalculator, numberOfCows int) (float64, error) {
	totalFodder, err := fc.FodderAmount(numberOfCows)
	if err != nil {
		return 0.0, err
	}

	fatteningFactor, err := fc.FatteningFactor()
	if err != nil {
		return 0.0, err
	}

	return (totalFodder / float64(numberOfCows) * fatteningFactor), nil
}

func ValidateInputAndDivideFood(fc FodderCalculator, numberOfCows int) (float64, error) {
	if numberOfCows <= 0 {
		return 0.0, errors.New("invalid number of cows")
	}

	return DivideFood(fc, numberOfCows)
}

func ValidateNumberOfCows(numberOfCows int) *InvalidCowsError {
	if numberOfCows <= 0 {
		return &InvalidCowsError{numberOfCows: numberOfCows}
	}

	return nil
}
