package lasagna

// According to the recipe, the lasagna should be in the oven for 40 minutes.
const OvenTime = 40

// It usually takes 2 minutes to prepare a layer of lasagna.
const LayerPrepTime = 2

// RemainingOvenTime returns the remaining minutes based on how long the lasagna has already been in the oven.
func RemainingOvenTime(minutesInOvenNow int) int {
	return OvenTime - minutesInOvenNow
}

// PreparationTime calculates the time needed to prepare the lasagna based on the number of layers.
func PreparationTime(numberOfLayers int) int {
	return numberOfLayers * LayerPrepTime
}

// ElapsedTime calculates the time elapsed cooking the lasagna.
// This time includes the preparation time and the time the lasagna has been baking in the oven.
func ElapsedTime(numberOfLayers, minutesInOvenNow int) int {
	return PreparationTime(numberOfLayers) + minutesInOvenNow
}
