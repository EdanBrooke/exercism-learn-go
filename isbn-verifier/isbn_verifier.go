package isbn

import (
	"strconv"
	"unicode"
)

func IsValidISBN(isbn string) bool {
	var digits = []int{}
	for i, r := range isbn {
		switch {
		case unicode.IsNumber(r):
			digitAsInt, _ := strconv.Atoi(string(r))
			digits = append(digits, digitAsInt)
		case r == '-':
			continue
		case r == 'X' && i+1 == len(isbn):
			digits = append(digits, 10)
		default:
			return false
		}
	}

	// All valid ISBN-10 numbers contain 10 digits
	if len(digits) != 10 {
		return false
	}

	var x int
	for i := 0; i < 10; i++ {
		x += digits[i] * (10 - i)
	}

	return x%11 == 0
}
