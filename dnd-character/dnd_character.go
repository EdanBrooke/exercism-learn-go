package dndcharacter

import (
	"math"
	"math/rand"
)

var diceNumbers = [...]int{1, 2, 3, 4, 5, 6}

type Character struct {
	Strength     int
	Dexterity    int
	Constitution int
	Intelligence int
	Wisdom       int
	Charisma     int
	Hitpoints    int
}

// We don't actually need to do a "dice roll", we could just do a random of between 3 and 18.
// That's not as fun, though.
func RollDice() uint8 {
	return uint8(diceNumbers[rand.Intn(len(diceNumbers))])
}

// Modifier calculates the ability modifier for a given ability score
func Modifier(score int) int {
	var modifier float64 = (float64(score) - 10) / 2
	if modifier > 0 {
		modifier = modifier - 0.5
	}
	return int(math.Round(modifier))
}

// Ability uses randomness to generate the score for an ability
func Ability() (score int) {
	var rolls []uint8
	// Roll the dice four times
	for len(rolls) < 4 {
		rolls = append(rolls, RollDice())
	}

	// Calculate the lowest roll
	var lowestRoll uint8 = min(rolls[0], rolls[1], rolls[2], rolls[3])

	for index, roll := range rolls {
		if roll == lowestRoll && len(rolls) == 4 {
			rolls = append(rolls[:index], rolls[index+1:]...)
		}
	}

	for _, roll := range rolls {
		score = score + int(roll)
	}

	return
}

// GenerateCharacter creates a new Character with random scores for abilities
func GenerateCharacter() (c Character) {
	c.Strength = Ability()
	c.Dexterity = Ability()
	c.Intelligence = Ability()
	c.Wisdom = Ability()
	c.Charisma = Ability()
	c.Constitution = Ability()
	c.Hitpoints = Modifier(c.Constitution) + 10
	return
}
