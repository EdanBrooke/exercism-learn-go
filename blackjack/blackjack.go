package blackjack

// ParseCard returns the integer value of a card following blackjack ruleset.
func ParseCard(card string) int {
	switch card {
	case "ace":
		return 11
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	case "ten":
		return 10
	case "jack":
		return 10
	case "queen":
		return 10
	case "king":
		return 10
	default:
		return 0
	}
}

// FirstTurn returns the decision for the first turn, given two cards of the
// player and one card of the dealer.
func FirstTurn(card1, card2, dealerCard string) string {
	card1Value := ParseCard(card1)
	card2Value := ParseCard(card2)
	totalValue := card1Value + card2Value
	dealerCardValue := ParseCard(dealerCard)
	switch {
	case totalValue == 22:
		// Player has a pair of aces.
		// Split.
		return "P"
	case totalValue == 21:
		// Player has a Blackjack.
		if dealerCard != "ace" && dealerCardValue != 10 {
			// Dealer doesn't have an ace or a figure of ten.
			// Automatically win.
			return "W"
		}
		// Dealer has an ace or a figure of ten.
		// Stand.
		return "S"
	case totalValue >= 17 && totalValue <= 20:
		// Stand.
		return "S"
	case totalValue >= 12 && totalValue <= 16:
		if dealerCardValue >= 7 {
			// Hit.
			return "H"
		}
		// Stand.
		return "S"
	default:
		// Hit.
		return "H"
	}
}
